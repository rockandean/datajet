!PROGRAM TEST
!  use module_test
!  implicit none
!  type(testType) :: myTest
!  myTest%key=5
!  myTest%next=2
!  myTest%value=int(33432,4)
!  call print_testType(myTest)
!END PROGRAM TEST
PROGRAM PPFREAD
use readmodule

  implicit none
  integer       :: l
!  integer(4),public    :: iishot
  integer(4)   ::  shot(50)
!  character*4,public   :: idda
!    character(len=8)  :: idda !! auxiliar name to keep public
 !       integer(4)    :: iishot !! auxiliar name to keep public 
!  CALL PPFUID("JETPPF","R")
 !   do l=1,50
 !   shot(l)=0
 !   end do
    open(unit=1,file='JET_shots.txt')
   ! READ(1,*)shot
 !   m=1
 !   while (shot(m).NE.0)
 !   m=m+1
 !   end while
    
    do l=1,6
    READ(1,*)shot(l)
    iishot=shot(l)
    ! SET here the DDA and include the dataypes you want in savedataModule.f95
    ! shot number is read from JET_shots.txt
    !
    idda='S3AD'  ! carbon ELM d alpha
!    idda='MAGN' 
!    idda='EDG8'  
!    idda='MNJ3'  ! IMK2  divertor coil
 !   idda='MAGF'
 !   idda='GASM'  !MAJR
 !   READ(*,*)idda

      CALL FC_PPFUID() !!> Passes the source PPF(JPF) and R(W), read(write) instruction.
      CALL FC_DDAINF() !!> save datatype info into a file: datatype.dat.
      CALL FC_SAVEDATA()
    end do
    close(1)
END PROGRAM PPFREAD
