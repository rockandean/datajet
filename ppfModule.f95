module verifyID
  implicit none

  type IDtype
    sequence
    character(len=8):: source      !!> identify source:PPF, JPF
    character(len=1):: RW          !!> read or write action when identify user
    character(len=8):: userid
  end type IDtype

  contains
    subroutine FC_PPFUID()
      implicit none
        type(IDtype) :: toRead !!> object with toRead%source and toRead%RW extension
        integer(2)   :: IERR
      
      !  Setup PPFUID call - specify the user name used for reading data and the R/W action  
!      toRead%source="JETPPF"
      toRead%source="JETPPF"
      toRead%RW="R"
      CALL PPFUID(toRead%source,toRead%RW)

      ! Initialise the PPF Systemm
      CALL PPFGO(0, 0, 0, IERR)
      IF (IERR.NE.0) THEN
        WRITE (*,*) 'PPFGO Error :',ierr
      else
        WRITE (*, *) 'Call made.'
      ENDIF
      
      ! call to find out what username was set
      CALL PPFGID(toRead%userid,toRead%RW)
      WRITE(*,*) 'Current ID: ', toRead%userid
    end subroutine FC_PPFUID

!so now I have source
end module verifyID

module readShot
  implicit none
  integer(4),parameter    :: mxdt=500
  integer(4),parameter    :: ndt=mxdt
  type shotname
  sequence
  integer(4)    :: ishot !!> shot number
  integer(4)    :: nwcom !! set to =0 if ddacom is not required
  integer(4)    :: ndt
  integer(4),public    :: lxtv(2,mxdt)
  integer(2)    :: IERR
  character(len=8)  :: dda
  character(len=4)  :: ddacom(ndt)
  character*4,public  :: dtnams(mxdt)
  end type shotname

  type(shotname),public    :: dda !!> dda%ishot, dda%nwcom, dda%dt ,dda%lxtv, dda%IERR
  character*4,public  :: idda !! auxiliar name to keep public
  integer(4),public    :: iishot !! auxiliar name to keep public 
  
  contains
    subroutine FC_DDAINF()
    
      implicit none
      character(len=2)  :: yn='y'
      integer(2)        :: J !!loop

      ! Get name and DDA for shot number
      !WHILE(yn.EQ.'n') 
 999  WRITE(*,*) 'Read the Shot number and DDA:'
 !     READ(*,*) iishot
 !     READ(*,*) idda
      dda%ishot=iishot
      dda%dda=idda  
      WRITE(*,*) 'Shot:', dda%ishot, 'DDA  ', dda%dda
      WRITE(*,*) 'is correct? [y/n]:'
    !  READ(*,*) yn
        if(yn.EQ.'y') then
        WRITE(*,*) 'Good, XD'
        else
        goto 999
        end if
      !END WHILE

    ! parameters
    dda%ndt=ndt
    CALL DDAINF(dda%ishot,dda%dda,dda%nwcom,dda%ddacom,dda%ndt,dda%dtnams,dda%lxtv,dda%IERR)
      if (dda%IERR.NE.0) THEN
        WRITE (*,*) 'DDAINF Error :',dda%IERR
      else
        WRITE (*, *) 'Reading correctly the DDA info...'
      end if  
      !
      WRITE(*,*) '**************** Output from DDAINF ***************'
     !
      OPEN(unit=4,file='datatype.dat')
      do J=1, dda%ndt
         WRITE(4,10) J,' DataType: ',dda%dtnams(J), '  X: ', dda%lxtv(1,J),'  T: ', dda%lxtv(2,J)
         WRITE(6,10) J,' DataType: ',dda%dtnams(J), '  X: ', dda%lxtv(1,J),'  T: ', dda%lxtv(2,J)
  10     format(i4,a10,a8,a6,i15,a,i15)    
      end do
      WRITE(*,*)' This has been save into "datatype.dat" file '
      CLOSE(4)  
    end subroutine FC_DDAINF
    
end module readShot








!module testType_mod
!  type testType
!    sequence
!    integer(4) :: key
!    integer(2) :: next
!    integer(4) :: value
!  end type
!end module

!module module_test
!  use testType_mod
!  implicit none
!  contains
!    subroutine print_testType(toPrint)
!      implicit none
!      type(testType), intent(in) :: toPrint
!      write(6,'("Key: ",i12," next: ",i5," value: ",i12)') &
!         & toPrint%key,toPrint%next,toPrint%value
!    end subroutine
!end module
