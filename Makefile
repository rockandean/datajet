#---------------------------------------------------------------------------
# F Calderon. 2013. Read data from source PPF or JPF makefile using PPF API.
#---------------------------------------------------------------------------
# This makefile works with the GNU make command, the one find on
# GNU/Linux systems and often called gmake on non-GNU systems.
FC=gfortran
FCFLAGS=-c -Wall -g -fbounds-check -std=f95
FCFLAGS += -I/usr/include
LDFLAGS = -lppf
SOURCES=   ppfModule.f95 savedataModule.f95 readmodule.f95 ppfReadMain.f95 
OBJECTS=$(SOURCES:.f95=.o)
EXECUTABLE=ReadData.out

 all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(FC) $(OBJECTS) -o $@ $(LDFLAGS)
%.o: %.f95
	$(FC) $(FCFLAGS) $< 
clean:
	rm -rf *.o $(EXECUTABLE) *.mod *.MOD *.dat
#appendix

## checking with single files
#all: ppfread 
#
#ppfread: ppfReadMain.o ppfModule.o
#	$(FC) ppfReadMain.o ppfModule.o -o $(EXECUTABLE)

#--- HINT: each *.o needs to include all    ---#
#--- *.o or *.f95 to complete the compiling ---#
#--- and linking!			    ---#

#ppfReadMain.o: ppfReadMain.f95 ppfModule.o
#	$(FC) $(FCFLAGS) ppfReadMain.f95  
#
#ppfModule.o: ppfModule.f95
#	$(FC) $(FCFLAGS) ppfModule.f95 
#

# some explaining

# $< is used in order to list only the first prerequisite (the source file) and not the additional prerequisites such as module or include files.
# The compiler to use.
#FC=gfortran
# options I'll pass to the compiler.
#FCFLAGS=-c -Wall -g -fbounds-check
# flags forall (e.g. look for system .mod files, required in gfortran)
#FCFLAGS += -I/usr/include

# libraries needed for linking, unused in the examples
#LDFLAGS = -li_need_this_lib


