module writedata
use readShot
implicit none
    
    type savedata_t
    integer(4)  :: ishot
    integer(4)  :: IERR
    integer(4)  :: irdat(13)
    integer(4)  :: iwdat(13)
    character*4 :: dda
    character*4 :: dtype
    character*4 :: ihdat(15)
    real(4),dimension(:),allocatable     :: tvector
    real(4),dimension(:),allocatable     :: ddata
    real(4),dimension(:),allocatable     :: xvector
    end type savedata_t
    contains
        
        subroutine FC_SAVEDATA()
        implicit none
        type(savedata_t)  :: dtype

        integer(2)  :: dtype_index
        integer(2)  :: ISEQ=0
        integer(4)  :: IERR
        integer  :: J,i
        character(len=1024)   ::  filename
        ! Call PPFGO with the specified shot and sequence number
        CALL PPFGO(iishot,ISEQ,0,IERR)  !!> needs a READ for ISEQ to get from screen
        IF (IERR.NE.0) THEN
        WRITE (6,*) 'PPFGO Error :',IERR
        END IF 
        
        do i=1,dda%ndt
        !Read index of wanted DataType to print in a file
!        WRITE(6,*)' Index of wanted DataType to print in a file( use printed order or check datatype.dat):'
!        READ(*,*) dtype_index
!        if (dda%dtnams(dtype_index) )
        dtype_index=i
        allocate(dtype%xvector(dda%lxtv(1,dtype_index)))
        allocate(dtype%ddata(dda%lxtv(1,dtype_index)*dda%lxtv(2,dtype_index)))
        allocate(dtype%tvector(dda%lxtv(2,dtype_index)))
        if( dda%dtnams(dtype_index).EQ.'AD36'.OR.&
           &dda%dtnams(dtype_index).EQ.'AD35'.OR.&
           &dda%dtnams(dtype_index).EQ.'AD34'.OR.&
           &dda%dtnams(dtype_index).EQ.'AD33'.OR.&
           &dda%dtnams(dtype_index).EQ.'IMK2'.OR.&
           &dda%dtnams(dtype_index).EQ.'MAJR'.OR.&
           &dda%dtnams(dtype_index).EQ.'TDAI'.OR.&
           &dda%dtnams(dtype_index).EQ.'TDAO'.OR.&
           &dda%dtnams(dtype_index).EQ.'TBEI'.OR.&
           &dda%dtnams(dtype_index).EQ.'TBEO') then
        dtype%ishot=iishot  !!> using public attribute of iishot: shot name
        dtype%dda=idda      !!> using public attribute of idda: DDA name
        dtype%dtype=dda%dtnams(dtype_index)   !!> number can be readed fro screen or datatype.dat file
        dtype%irdat(1)=0
        dtype%irdat(2)=0
        dtype%irdat(3)=0
        dtype%irdat(4)=0
        dtype%irdat(5)=dda%lxtv(1,dtype_index)*dda%lxtv(2,dtype_index)  !!> dimension of the DATA array
        dtype%irdat(6)=dda%lxtv(1,dtype_index)  !!> dimension of the X array
        dtype%irdat(7)=dda%lxtv(2,dtype_index)
        dtype%irdat(8)=0
        dtype%irdat(9)=0

          CALL PPFGET(dtype%ishot,dtype%dda,dtype%dtype,dtype%irdat,dtype%ihdat,dtype%iwdat&
          &,dtype%ddata,dtype%xvector,dtype%tvector,dtype%IERR)
          if (dtype%IERR.NE.0) THEN
          WRITE (*,*) 'PPFGET Error :',dtype%IERR
          else
          WRITE (*, *) 'Reading correctly the DataType info...'
          
          end if  
        !
        WRITE(*,*) '**************** Output from DataType stored in data.dat ***************'
        !
       
        write(filename,'(i6,a6)') iishot
        OPEN(unit=7,file=trim(adjustl(filename))//"_"//dda%dtnams(dtype_index)//".dat")
        WRITE(7,*) '  index  ', '       time       ','           signal        '
        do J=1,dda%lxtv(2,dtype_index)
         !WRITE(7,10) 'DataType: ',dda%dtnams(J), '  X: ', dda%lxtv(1,J),'  T: ', dda%lxtv(2,J)
         WRITE(7,20) J,dtype%tvector(J),dtype%ddata(J)
 !        WRITE(6,20) J,dtype%ddata(J),dtype%tvector(J)
 ! 10     format(a10,a8,a10)    
  20     format(i8,1x,f16.12,1x,f28.0)    
        end do
      WRITE(*,*)' This has been save into ',trim(adjustl(filename))//"_"//dda%dtnams(dtype_index)//".dat",' file.'
      CLOSE(7)
      end if
      deallocate(dtype%xvector)
      deallocate(dtype%ddata)
      deallocate(dtype%tvector)
      end do

    end subroutine FC_SAVEDATA
end module writedata

