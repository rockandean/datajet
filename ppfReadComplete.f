* *****                                                        *****
* ***** PPF Tutorial Answers: PPF Reading Excercises - Fortran *****
* *****                                                        *****
*
      PROGRAM ppfReadComplete
*
* Example program to read a PPF datatype (MAGN/IPLA) for a shot
* input by the user

*name of user specified for reading PPF Data
      CHARACTER setuid*8
*
* Shot no., error code, arrays for PPFGET, error code for PPFERR
      INTEGER iishot,ISHOT,IERR,IRDAT(13),IWDAT(13), INFDAT(12)

* Dimensions of data, X-vector and T-vector arrays
* (appropriate for MAGN/IPLA)
      INTEGER NDMAX,NTMAX,NXMAX
      PARAMETER (NTMAX=1,NXMAX=1024,NDMAX=NXMAX*NTMAX)

*
* DDA, Dat

*Type, output parameter array for PPFGET
      CHARACTER*4 DDA,DTYPE,IHDAT(15)
*,CDDA, ICOM    
*
* Output data, x vector and T vector arrays for PPFGET
      REAL*4 DATAs(NDMAX),X(NXMAX),T(NTMAX)

*
* ppferr parameters
      CHARACTER*80 MSG
     
*
* Variables for use in PDSTD
      INTEGER ITIME, IDATE

* Variables for use in PPFINF
      INTEGER LUN, NCMAX, IDAT(12), NDDA
      CHARACTER*4 ICOM(100), CDDA(5)

* Variables for use in DDAINF
      INTEGER NWCOM, NDT, LXTV(2, 100)
      CHARACTER*4 DDACOM(100), DTNAMS(100)

* Variables for use in PPFGSF
      INTEGER ISTAT
*
* Setup PPFUID call - specify the user name used for reading 
* data
*     
      CALL PPFUID("JETPPF","R")

* Initialise the PPF Systemm
*
      CALL PPFGO(0, 0, 0, IERR)
      IF (IERR.NE.0) THEN
        WRITE (6,*) 'PPFGO Error :',ierr
        GOTO 999
      ENDIF 
*
* Call ppfgid to find out what username was set
*
      CALL PPFGID(setuid,"R")
      write(6,*) "Read uid =",setuid

* Get date and time for shot number #
      WRITE(*,*) 'Read the Shot number'
      READ(*,*) iishot
*     
      ISHOT=iishot
      
      CALL PDSTD(ISHOT, ITIME, IDATE, IERR)
      IF (IERR.NE.0) THEN
        WRITE (6,*) 'PPFSTD Error :',ierr
        GOTO 999
      ENDIF 

      WRITE(6,*) '\n\n***** Results from PDSTD *****'
      WRITE(6,*) 'Shot: ', ISHOT
      WRITE(6,*) 'Time: ', ITIME
      WRITE(6,*) 'Date: ', IDATE

*Get list of DDA names from PPFINF
      ISHOT = iishot
      ISEQ = 0
      LUN = 0
      NCMAX = 0
      NDDA = 30
* Get list of number sequence in the shot PPF
      DEVELOP=0
      IF (DEVELOP.NE.0) THEN
      CALL PPFDDA(ISHOT,DDA,ISEQ,NSEQ,IERR)
      IF (IERR.NE.0) THEN
        WRITE(6,*) 'PPFDDA Error :', ierr
        GOTO 999
      ENDIF
      ELSE
      ENDIF

      CALL PPFGO(ISHOT, ISEQ, 0, IERR)
      IF (IERR.NE.0) THEN
        WRITE (6,*) 'PPFGO Error :',ierr
        GOTO 999
      ENDIF 
        print*, 'hola'
      CALL PPFINF(ISHOT, ISEQ, LUN, NCMAX, IWDAT, ICOM, IERR,
     *NDDA, CDDA)
      IF (IERR.NE.0) THEN
        WRITE(6,*) 'PPFINF Error :',ierr
        GOTO 999
      ENDIF

      WRITE(6,*) '\n\n***** Output from PPFINF *****'
      DO J=1, NDDA
         WRITE(6,*) 'DDA: ', CDDA(J)
      ENDDO
      
* Get list of Datatypes associated with second DDA
      NDT = 100
      NWCOM = 0

 
      CALL DDAINF(ISHOT, CDDA(2), NWCOM, DDACOM, NDT, DTNAMS, LXTV,
     *IERR)
      IF (IERR.NE.0) THEN
         WRITE(6,*) 'DDAINF Error :',ierr
         GOTO 999
      ENDIF
      
      WRITE(6,*) '\n\n***** Output from DDAINF *****'
      
      DO J=1, NDT
         WRITE(6,*) 'DType: ', DTNAMS(J), '  X: ', LXTV(1,J),
     *'  T: ', LXTV(2,J), '  Data: ', LXTV(1,J)*LXTV(2,J)
      ENDDO

* Get DDA and Datatype status flags for LIDR/NE
      CALL PPFGSF(ISHOT, 0, 'LIDR', '', ISTAT, IERR)
      IF (IERR.NE.0) THEN
         WRITE(6,*) 'DDAINF Error :',ierr
         GOTO 999
      ENDIF
      WRITE(6,*) '\n\n***** Output from PPFGSF *****'
      WRITE(6,*) 'Status flag for LIDR: ', ISTAT
      
      CALL PPFGSF(ISHOT, 0, 'LIDR', 'NE  ', ISTAT, IERR)
      IF (IERR.NE.0) THEN
         WRITE(6,*) 'DDAINF Error :',ierr
         GOTO 999
      ENDIF
      WRITE(6,*) 'Status flag for LIDR/NE: ', ISTAT


*
* Setup general parameters - usually these would be found
* by calling other PPF Routines
*
      ISHOT = iishot
      ISEQ = 0
      DDA = 'GASM'
      DTYPE = 'MAJR'
    
* Call PPFGO with the specified shot and sequence number
      CALL PPFGO(ISHOT,ISEQ,0,IERR)
      IF (IERR.NE.0) THEN
        WRITE (6,*) 'PPFGO Error :',ierr
        GOTO 999
      ENDIF 
*
* Setup PPFGET call - 
* array sizes to hold retrieved X, T and DATA vectors
* Under normal usage these will be found using calls to other 
* PPF Routines
*
      IRDAT(1) = 0
      IRDAT(2) = 10

      IRDAT(3) = 0
      IRDAT(4) = 1
 
      IRDAT(6) = IRDAT(2) - IRDAT(1)
      IRDAT(7) = IRDAT(4) - IRDAT(3)
      
      IRDAT(5) = IRDAT(6) * IRDAT(7)

*
* Set flags to return x- and t-vectors
*
      IRDAT(8) = 0
      IRDAT(9) = 0
   
 10   CALL PPFGET(ISHOT,DDA,DTYPE,IRDAT,IHDAT,IWDAT,DATAs,X,T,IERR)
      IF (IERR.NE.0) THEN
         CALL PPFERR("PPFGET",IERR,MSG,IERRER)
         WRITE (6,*) MSG
         GOTO 999
      ENDIF
*
* Output some info about the retrieved data
*
      WRITE(6,*) '\n\n***** Results from PPFGET *****'
    

       DO J=1,IWDAT(7)
         DO I=1,IWDAT(6)
            WRITE(6,*)X(I),T(J),DATAS((J-1)*IWDAT(6) + I)
         ENDDO
      ENDDO

999   CONTINUE
      END
